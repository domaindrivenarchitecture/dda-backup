# Application Backup for k8s

![](backup.svg)

* we use restic to produce small & encrypted backups
* backups are stored in s3
* backup is scheduled at `schedule: "10 23 * * *"`
* backup supports file or postgres-db-dump backup.
* supported restic usecases are backup, restore, change password, list snapshots, support retentions

## Parameter determined by k8s-application

1. name of application deployments **deployment-name** to scale down during backup / restore
2. namespace of application deployments **deployment-namespace**

## backup / restore

### Scale application down

1. Scale [deployment-name] deployment down:   
   `kubectl -n [deployment-namespace] scale deployment [deployment-name] --replicas=0`

### Manual backup

1. Scale backup-restore deployment up:   
   `kubectl -n [deployment-namespace] scale deployment backup-restore --replicas=1`
2. exec into pod and execute restore pod   
   `kubectl -n [deployment-namespace] exec -it backup-restore -- backup.bb`
3. Scale backup-restore deployment down:   
  `kubectl -n [deployment-namespace] scale deployment backup-restore --replicas=0`

### Manual restore

1. Scale backup-restore deployment up:   
   `kubectl -n [deployment-namespace] scale deployment backup-restore --replicas=1`
3. exec into pod and execute restore pod   
   `kubectl -n [deployment-namespace] exec -it backup-restore -- restore.bb`
4. Scale backup-restore deployment down:   
  `kubectl -n [deployment-namespace] scale deployment backup-restore --replicas=0`

### Scale application up

1. Scale [deployment-name] deployment up:   
   `kubectl -n [deployment-namespace] scale deployment [deployment-name] --replicas=1`

## Change Password

1. Check restic-new-password env is set in backup deployment   
   ```
   kind: Deployment
   metadata:
     name: backup-restore
   spec:
       spec:
         containers:
         - name: backup-app
           env:
           - name: RESTIC_NEW_PASSWORD_FILE
             value: /var/run/secrets/backup-secrets/restic-new-password
   ```
2. Add restic-new-password to secret   
   ```
   kind: Secret
   metadata:
     name: backup-secret
   data:
     restic-password: old
     restic-new-password: new
   ```
3. Scale backup-restore deployment up:   
   `kubectl -n [deployment-namespace] scale deployment backup-restore --replicas=1`
4. exec into pod and execute restore pod   
   `kubectl -n [deployment-namespace] exec -it backup-restore -- change-password.bb`
5. Scale backup-restore deployment down:   
  `kubectl -n [deployment-namespace] scale deployment backup-restore --replicas=0`
6. Replace restic-password with restic-new-password in secret   
   ```
   kind: Secret
   metadata:
     name: backup-secret
   data:
     restic-password: new
   ```
