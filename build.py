from os import environ
from subprocess import run
from pybuilder.core import init, task
from ddadevops import *

default_task = "dev"
name = "dda-backup"
MODULE = "not-used"
PROJECT_ROOT_PATH = "."


@init
def initialize(project):
    project.build_depends_on("ddadevops>=4.7.0")

    input = {
        "name": name,
        "module": MODULE,
        "stage": "notused",
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": [],
        "mixin_types": ["RELEASE"],
        "release_primary_build_file": "deps.edn",
        "release_secondary_build_files": [
            "infrastructure/backup/build.py",
        ],
        "release_artifact_server_url": "https://repo.prod.meissa.de",
        "release_organisation": "meissa",
        "release_repository_name": name,
        "release_artifacts": [],
    }

    build = ReleaseMixin(project, input)
    build.initialize_build_dir()


@task
def test(project):
    run("make test", shell=True, check=True)


@task
def package(project):
    run("make build-jar", shell=True, check=True)


@task
def upload(project):
    run("make deploy", shell=True, check=True)


@task
def patch(project):
    linttest(project, "PATCH")
    release(project)


@task
def minor(project):
    linttest(project, "MINOR")
    release(project)


@task
def major(project):
    linttest(project, "MAJOR")
    release(project)


@task
def dev(project):
    linttest(project, "NONE")


@task
def prepare(project):
    build = get_devops_build(project)
    build.prepare_release()


@task
def tag(project):
    build = get_devops_build(project)
    build.tag_bump_and_push_release()


@task
def publish_artifacts(project):
    build = get_devops_build(project)
    build.publish_artifacts()


def release(project):
    prepare(project)
    tag(project)


def linttest(project, release_type):
    build = get_devops_build(project)
    build.update_release_type(release_type)
    test(project)
