# dda-backup

[![Clojars Project](https://img.shields.io/clojars/v/org.domaindrivenarchitecture/dda-backup.svg)](https://clojars.org/org.domaindrivenarchitecture/dda-backup) [![pipeline status](https://gitlab.com/domaindrivenarchitecture/dda-backup/badges/master/pipeline.svg)](https://gitlab.com/domaindrivenarchitecture/dda-backup/-/commits/main)

[<img src="https://domaindrivenarchitecture.org/img/delta-chat.svg" width=20 alt="DeltaChat"> chat over e-mail](mailto:buero@meissa-gmbh.de?subject=community-chat) | [<img src="https://meissa.de/images/parts/contact/mastodon36_hue9b2464f10b18e134322af482b9c915e_5501_filter_14705073121015236177.png" width=20 alt="M"> meissa@social.meissa-gmbh.de](https://social.meissa-gmbh.de/@meissa) | [Blog](https://domaindrivenarchitecture.org) | [Website](https://meissa.de)

Images are available at https://hub.docker.com/r/domaindrivenarchitecture/dda-backup

## Usage example

```clojure
#!/usr/bin/env bb

(require '[babashka.tasks :as tasks]
         '[dda.backup.restic :as rc]
         '[dda.backup.backup :as bak]
         '[dda.backup.restore :as rs])

(def config {:restic-repo
             :debug true
             :restic-repository "/restic-repo"
             :password-file "/tmp/file_password"
             :new-password-file "/tmp/file_new_password"
             :backup-path "files"
             :execution-directory "/var/backups/"
             :files ["test-backup"]
             :restore-target-directory "test-restore"})

(defn prepare!
  []
  (tasks/shell "mkdir" "-p" "/var/backups/test-backup")
  (spit "/var/backups/test-backup/file" "I was here")
  (tasks/shell "mkdir" "-p" "/var/backups/test-restore"))

(defn restic-repo-init!
  []
  (println "\nrestic-repo-init!")
  (rc/init! config))

(defn restic-backup!
  []
  (println "\nrestic-backup!")
  (bak/backup-file! config))

(defn list-snapshots!
  []
  (println "\nlist-snapshots!")
  (rc/list-snapshots! config))

(defn restic-restore!
  []
  (println "\nrestic-restore!")
  (rs/restore-file! config))

(defn change-password!
  []
  (println "\nchange-password!")
  (rc/change-password! config))

(defn restic-backup-with-new!
  []
  (println "\nrestic-backup with new!")
  (bak/backup-file! (merge {:password-file "/tmp/file_new_password"} config)))

(defn list-snapshots-with-new!
  []
  (println "\nlist-snapshots with new!")
  (rc/list-snapshots! (merge {:password-file "/tmp/file_new_password"} config)))


(prepare!)
(restic-repo-init!)
(restic-backup!)
(list-snapshots!)
(restic-restore!)
(change-password!)
(restic-backup!)
(list-snapshots!)
(restic-restore!)
(restic-backup-with-new!)
(list-snapshots-with-new!)

```

## Real Life Examples

see:

* https://repo.prod.meissa.de/meissa/c4k-taiga/src/branch/main/infrastructure/backup/image/resources
* https://repo.prod.meissa.de/meissa/c4k-forgejo/src/branch/main/infrastructure/backup/image/resources

## Development & mirrors

Development happens at: https://repo.prod.meissa.de/meissa/dda-backup

Mirrors are:

* https://codeberg.org/meissa/dda-backup (Issues and PR)
* https://gitlab.com/domaindrivenarchitecture/dda-backup (CI)

For more details about our repository model see: https://repo.prod.meissa.de/meissa/federate-your-repos

## License
Copyright © 2024, 2025 meissa GmbH
Published under [apache2.0 license](LICENSE.md)
Pls. find licenses of our subcomponents [here](doc/SUBCOMPONENT_LICENSE)
