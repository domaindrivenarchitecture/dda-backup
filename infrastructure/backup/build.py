from os import environ
from datetime import datetime
from pybuilder.core import task, init
from ddadevops import *
from ddadevops.infrastructure import FileApi, ExecutionApi
import logging

name = 'dda-backup'
MODULE = 'NOT_SET'
PROJECT_ROOT_PATH = '../..'
version = "5.4.1-dev"


@init
def initialize(project):
    image_tag = version
    if "dev" in image_tag:
        image_tag += datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

    input = {
        "name": name,
        "module": MODULE,
        "stage": "notused",
        "project_root_path": PROJECT_ROOT_PATH,
        "build_types": ["IMAGE"],
        "mixin_types": [],
        "image_naming": "NAME_ONLY",
        "image_tag": f"{image_tag}",
    }

    project.build_depends_on("ddadevops>=4.7.0")

    build = DevopsImageBuild(project, input)
    build.initialize_build_dir()
    file_api = FileApi()
    e_api = ExecutionApi()
    e_api.execute(f"mkdir {build.build_path()}/image/local/")
    file_api.cp("../../deps.edn", f"{build.build_path()}/image/local/")
    file_api.cp_recursive("../../src", f"{build.build_path()}/image/local/src")
    e_api.execute(f"mkdir {build.build_path()}/test/local/")
    file_api.cp("../../deps.edn", f"{build.build_path()}/test/local/")
    file_api.cp_recursive("../../src", f"{build.build_path()}/test/local/src")


@task
def image(project):
    build = get_devops_build(project)
    build.image()

@task
def test(project):
    build = get_devops_build(project)
    build.test()

@task
def drun(project):
    build = get_devops_build(project)
    build.drun()


@task
def publish(project):
    build = get_devops_build(project)
    build.dockerhub_login()
    build.dockerhub_publish()
