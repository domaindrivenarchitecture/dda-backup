# changing password on restic repository

## config to use

To change the password add new-password-file to config.

```clojure
 {:password-file "/restic-pwd"
  :new-password-file "/new-restic-pwd"}
```

## restic: decide which password to choose

If there is a new-password-file defined, decide witch to use:

```mermaid
stateDiagram-v2
    state new? <<choice>>
    state "restic --password-file /new-pwd check" as check_new
    state new_works? <<choice>>

    [*] --> containsNewPassword?
    containsNewPassword? --> new?
    new? --> check_new: yes
    new? --> use_old: no
    check_new --> new_works?
    new_works? --> use_new: yes
    new_works? --> use_old: no
```

# Process to change password in k8s

```mermaid
sequenceDiagram
    actor d as DevOps
    participant s as BackupSecret
    participant a as ApplicationDeployment
    participant b as BackupDeployment
    
    d ->> s: add new-password-file
    d ->> a: scale down
    d ->> b: scale up
    d ->> b: shell into
    activate b
    b ->> b: call change password
    deactivate b
    d ->> s: replace password-file with new-password-file
    d ->> b: scale down
    d ->> a: scale up
```
