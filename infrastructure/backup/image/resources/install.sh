#!/bin/bash

set -exo pipefail

function babashka_install() {
    babashka_version="1.3.189"
    curl -SsLo /tmp/babashka-${babashka_version}-linux-amd64.tar.gz https://github.com/babashka/babashka/releases/download/v${babashka_version}/babashka-${babashka_version}-linux-amd64.tar.gz
    curl -SsLo /tmp/checksum https://github.com/babashka/babashka/releases/download/v${babashka_version}/babashka-${babashka_version}-linux-amd64.tar.gz.sha256
    echo "  /tmp/babashka-$babashka_version-linux-amd64.tar.gz"|tee -a /tmp/checksum
    sha256sum -c --status /tmp/checksum
    tar -C /tmp -xzf /tmp/babashka-${babashka_version}-linux-amd64.tar.gz 
    install -m 0700 -o root -g root /tmp/bb /usr/local/bin/
}

function main() {
    {
        upgradeSystem
        apt-get install -qqy ca-certificates curl gnupg postgresql-client-16 restic openjdk-21-jre-headless nano
        curl -Ss --fail https://www.postgresql.org/media/keys/ACCC4CF8.asc | gpg --dearmor | tee /etc/apt/trusted.gpg.d/postgresql-common_pgdg_archive_keyring.gpg
        sh -c 'echo "deb [signed-by=/etc/apt/trusted.gpg.d/postgresql-common_pgdg_archive_keyring.gpg] https://apt.postgresql.org/pub/repos/apt jammy-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
        babashka_install      
    } > /dev/null

    update-ca-certificates
    install -m 0700 -o root -g root /tmp/init-bb.bb /usr/local/bin/
    install -m 0600 -o root -g root /tmp/bb.edn /usr/local/bin/
    cleanupDocker
}

source /tmp/install_functions_debian.sh
DEBIAN_FRONTEND=noninteractive DEBCONF_NOWARNINGS=yes main
