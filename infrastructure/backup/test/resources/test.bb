#!/usr/bin/env bb

(require '[babashka.tasks :as tasks]
         '[dda.backup.config :as cfg]
         '[dda.backup.restic :as rc]
         '[dda.backup.postgresql :as pg]
         '[dda.backup.backup :as bak]
         '[dda.backup.restore :as rs])

(def config (cfg/read-config "/tmp/config.edn"))

(defn prepare!
  []
  (println config)
  (tasks/shell "mkdir" "-p" "/var/backups/test-backup")
  (spit "/var/backups/test-backup/file" "I was here")
  (tasks/shell "mkdir" "-p" "/var/backups/test-restore")
  (pg/create-pg-pass! (:db-config config)))

(defn restic-repo-init!
  []
  (println "\nrestic-repo-init!")
  (rc/init! (:file-config config))
  (rc/init! (merge (:db-config config) (:dry-run config)))
  (rc/init! (merge (:db-roles-config config) (:dry-run config))))

(defn restic-backup!
  []
  (println "\nrestic-backup!")
  (bak/backup-file! (:file-config config))
  (bak/backup-db-roles! (merge (:db-roles-config config) (:dry-run config)))
  (bak/backup-db! (merge (:db-config config) (:dry-run config))))

(defn list-snapshots!
  []
  (println "\nlist-snapshots!")
  (rc/list-snapshots! (:file-config config))
  (rc/list-snapshots! (merge (:db-roles-config config) (:dry-run config)))
  (rc/list-snapshots! (merge (:db-config config) (:dry-run config))))

(defn restic-restore!
  []
  (println "\nrestic-restore!")
  (rs/restore-file! (:file-config config))
  (pg/drop-create-db! (merge (:db-config config) (:dry-run config)))
  (rs/restore-db-roles! (merge (:db-roles-config config) (:dry-run config)))
  (rs/restore-db! (merge (:db-config config) (:dry-run config))))

(defn change-password!
  []
  (println "\nchange-password!")
  (rc/change-password! (:file-config config)))

(defn restic-backup-with-new!
  []
  (println "\nrestic-backup with new!")
  (bak/backup-file! (:file-config-with-new config)))

(defn list-snapshots-with-new!
  []
  (println "\nlist-snapshots with new!")
  (rc/list-snapshots! (:file-config-with-new config)))


(prepare!)
(restic-repo-init!)
(restic-backup!)
(list-snapshots!)
(restic-restore!)
(change-password!)
(restic-backup!)
(list-snapshots!)
(restic-restore!)
(restic-backup-with-new!)
(list-snapshots-with-new!)
