(ns dda.backup.core.domain-test
  (:require
   [clojure.test :refer [deftest is are testing run-tests]]
   [clojure.spec.alpha :as s]
   [dda.backup.core.domain :as cut]))

(deftest should-verify-command
  (is (= {:app "restic", :params ["-r" "repo/dir" "-v" "init" "--cacert" "ca"]}
         (s/conform ::cut/command ["restic" "-r" "repo/dir" "-v" "init" "--cacert" "ca"]))))

(deftest should-verify-commands
  (is (= [{:app "ls"}
          {:app "restic", :params ["-r" "repo/dir" "-v" "init" "--cacert" "ca"]}]
         (s/conform ::cut/commands [["ls"]
                                    ["restic" "-r" "repo/dir" "-v" "init" "--cacert" "ca"]]))))
