(ns dda.backup.postgresql.domain-test
  (:require
   [clojure.test :refer [deftest is are testing run-tests]]
   [clojure.spec.test.alpha :as st]
   [dda.backup.postgresql.domain :as cut]))

(st/instrument `cut/pgpass)
(st/instrument `cut/db-drop-command)
(st/instrument `cut/db-create-command)

(deftest should-calculate-pgpass
  (is (= "localhost:5432:mydb:user:password\nlocalhost:5432:template1:user:password\n"
         (cut/pgpass {:restic-repository "repo"
                      :backup-path "dir-at-repo"
                      :pg-host "localhost"
                      :pg-port 5432
                      :pg-db "mydb"
                      :pg-user "user"
                      :pg-password "password"}))))

(deftest should-calculate-db-drop-command
  (is (= [["psql" "-d" "template1" "-h" "localhost" "-p" "5432" "-U" "user"
           "--no-password" "-c" "DROP DATABASE \"mydb\";"]]
         (cut/db-drop-command {:restic-repository "repo"
                                      :backup-path "dir-at-repo"
                                      :pg-host "localhost"
                                      :pg-port 5432
                                      :pg-db "mydb"
                                      :pg-user "user"
                                      :pg-password "password"}))))

(deftest should-calculate-db-create-command
  (is (= [["psql" "-d" "template1" "-h" "localhost" "-p" "5432" "-U" "user"
           "--no-password" "-c" "CREATE DATABASE \"mydb\";"]]
         (cut/db-create-command {:restic-repository "repo"
                                      :backup-path "dir-at-repo"
                                      :pg-host "localhost"
                                      :pg-port 5432
                                      :pg-db "mydb"
                                      :pg-user "user"
                                      :pg-password "password"}))))
