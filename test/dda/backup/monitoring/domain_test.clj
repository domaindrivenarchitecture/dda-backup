(ns dda.backup.monitoring.domain-test
  (:require
   [clojure.test :refer [deftest is are testing run-tests]]
   [clojure.spec.test.alpha :as st]
   [dda.backup.monitoring.domain :as cut]))

(st/instrument `cut/collect-metrics)

(deftest should-collect-metrics
  (is (= "\n"
         (cut/collect-metrics {:url "url"
                               :name "name"
                               :namespace "default"
                               :metrics {}})))
  (is (= "metric1{namespace=\"default\"} 1\nmetric2{namespace=\"default\"} text\n"
         (cut/collect-metrics {:url "url"
                               :name "name"
                               :namespace "default"
                               :metrics {:metric1 1
                                         :metric2 "text"}}))))
