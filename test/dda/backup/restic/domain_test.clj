(ns dda.backup.restic.domain-test
  (:require
   [clojure.test :refer [deftest is are testing run-tests]]
   [clojure.spec.test.alpha :as st]
   [dda.backup.restic.domain :as cut]))

(st/instrument `cut/repo-command)
(st/instrument `cut/init-repo-command)
(st/instrument `cut/unlock-repo-command)
(st/instrument `cut/forget-command)

(deftest should-calculate-repo-command
  (is (=  [{:dir "dir" :err :string}
           "restic"
           "-r"
           "repo/dir"
           "-v"
           "--cacert"
           "ca"
           "backup"
           "file1"
           "file2"]
          (cut/repo-command {:certificate-file "ca"
                             :execution-directory "dir"
                             :restic-repository "repo"
                             :backup-path "dir"
                             :days-to-keep 39
                             :months-to-keep 3}
                            ["backup" "file1" "file2"] 
                            true)))
   (is (=  [{:dir "dir"}
           "restic"
           "-r"
           "repo/dir"
           "-v"
           "--cacert"
           "ca"
           "backup"
           "file1"
           "file2"]
          (cut/repo-command {:certificate-file "ca"
                             :execution-directory "dir"
                             :restic-repository "repo"
                             :backup-path "dir"
                             :days-to-keep 39
                             :months-to-keep 3}
                            ["backup" "file1" "file2"] false)))
  (is (= [{:err :string} "restic" "-r" "repo/dir" "-v" "--cacert" "ca" "snapshots"]
         (cut/repo-command {:certificate-file "ca"
                            :restic-repository "repo"
                            :backup-path "dir"
                            :days-to-keep 39
                            :months-to-keep 3}
                           ["snapshots"] true)))
  (is (= ["restic" "-r" "repo/dir" "-v" "--password-file" "password" "snapshots"]
         (cut/repo-command {:password-file "password"
                            :restic-repository "repo"
                            :backup-path "dir"
                            :days-to-keep 39
                            :months-to-keep 3}
                           ["snapshots"] false)))
  (is (= ["restic" "-r" "repo/dir" "-v" "snapshots"]
         (cut/repo-command {:restic-repository "repo"
                            :backup-path "dir"
                            :days-to-keep 39
                            :months-to-keep 3}
                           ["snapshots"] false))))

(deftest should-calculate-init-repo-command
  (is (= [["restic" "-r" "repo/dir" "-v" "init"]]
         (cut/init-repo-command {:restic-repository "repo"
                                 :backup-path "dir"
                                 :days-to-keep 39
                                 :months-to-keep 3}))))

(deftest should-calculate-unlock-repo-command
  (is (= [["restic" "-r" "repo/dir" "-v" "--cleanup-cache" "unlock"]]
         (cut/unlock-repo-command {:restic-repository "repo"
                                   :backup-path "dir"
                                   :days-to-keep 39
                                   :months-to-keep 3}))))

(deftest should-calculate-forget-command
  (is (= [["restic" "-r" "repo/dir" "-v" "forget"
           "--group-by" "" "--keep-last" "1"
           "--keep-daily" "39" "--keep-monthly" "3" "--prune"]]
         (cut/forget-command {:restic-repository "repo"
                              :backup-path "dir"
                              :days-to-keep 39
                              :months-to-keep 3}))))

(deftest should-calculate-change-password-command
  (is (= [["restic"
           "-r"
           "repo/dir"
           "-v"
           "--new-password-file"
           "/new-pwd"
           "key"
           "passwd"]]
         (cut/change-password-command {:restic-repository "repo"
                                       :new-password-file "/new-pwd"
                                       :backup-path "dir"
                                       :days-to-keep 39
                                       :months-to-keep 3}))))

(deftest should-parse-check-error
  (is (= :not-initialized
         (cut/parse-check-error "Fatal: unable to open config file: stat /restic-repo/files/config: no such file or directory\nIs there a repository at the following location?\n/restic-repo/files" )
         ))
  (is (= :wrong-password
         (cut/parse-check-error "Fatal: wrong password or no key found\n")))
  (is (= :no-password
         (cut/parse-check-error "Resolving password failed: Fatal: /restic-pwd does not exist\n")))
  )
