(ns dda.backup.restore.domain-test
  (:require
   [clojure.test :refer [deftest is are testing run-tests]]
   [clojure.spec.test.alpha :as st]
   [dda.backup.restore.domain :as cut]))

(st/instrument `cut/restore-dir-command)
(st/instrument `cut/restore-db-command)

(deftest should-calculate-restore-dir-command
  (is (= [["rm" "-rf" "dir-to-backup"]
          ["restic"
           "-r"
           "repo/dir-at-repo"
           "-v"
           "restore"
           "latest"
           "--target"
           "dir-to-backup"]]
         (cut/restore-dir-command {:restic-repository "repo"
                                   :backup-path "dir-at-repo"
                                   :restore-target-directory "dir-to-backup"
                                   :days-to-keep 39
                                   :months-to-keep 3
                                   :snapshot-id "latest"})))
  (is (= [["rm" "-rf" "dir-to-backup/file" "dir-to-backup/folder/"]
          ["restic"
           "-r"
           "repo/dir-at-repo"
           "-v"
           "restore"
           "latest"
           "--target"
           "dir-to-backup"]]
         (cut/restore-dir-command {:restic-repository "repo"
                                   :backup-path "dir-at-repo"
                                   :restore-target-directory "dir-to-backup"
                                   :clean-up-elements ["file" "folder/"]
                                   :days-to-keep 39
                                   :months-to-keep 3
                                   :snapshot-id "latest"}))))

(deftest should-calculate-restore-db
  (is (= [["bash"
           "-c"
           (str "restic -r repo/dir-at-repo -v dump latest stdin | "
                "psql -d mydb -h localhost -p 5432 -U user --no-password")]]
         (cut/restore-db-command {:restic-repository "repo"
                                  :backup-path "dir-at-repo"
                                  :pg-host "localhost"
                                  :pg-port 5432
                                  :pg-db "mydb"
                                  :pg-user "user"
                                  :pg-password "password"
                                  :days-to-keep 39
                                  :months-to-keep 3
                                  :snapshot-id "latest"}))))

(deftest should-calculate-restore-db-roles
  (is (= [["bash"
           "-c"
           (str "restic -r repo/dir-at-repo -v dump latest stdin | "
                "psql -d template1 -h localhost -p 5432 -U user --no-password")]]
         (cut/restore-db-roles-command {:restic-repository "repo"
                                        :backup-path "dir-at-repo"
                                        :pg-host "localhost"
                                        :pg-port 5432
                                        :pg-db "mydb"
                                        :pg-user "user"
                                        :pg-password "password"
                                        :days-to-keep 39
                                        :months-to-keep 3
                                        :snapshot-id "latest"}))))
