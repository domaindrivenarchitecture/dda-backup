(ns dda.backup.backup.domain-test
  (:require
   [clojure.test :refer [deftest is are testing run-tests]]
   [clojure.spec.test.alpha :as st]
   [dda.backup.backup.domain :as cut]))

(st/instrument `cut/backup-files-command)
(st/instrument `cut/backup-role-command)
(st/instrument `cut/backup-db-command)

(deftest should-calculate-backup-files-command
  (is (= [[{:dir "dir-to-backup"}
           "restic"
           "-r"
           "repo/dir-at-repo"
           "-v"
           "backup"
           "file2"
           "file2"]]
         (cut/backup-files-command {:restic-repository "repo"
                                    :backup-path "dir-at-repo"
                                    :execution-directory "dir-to-backup"
                                    :days-to-keep 39
                                    :months-to-keep 3
                                    :files ["file2" "file2"]}))))

(deftest should-calculate-backup-role-command
  (is (= [["bash" "-c" (str "pg_dumpall -h localhost -p 5432 -U user --no-password --roles-only | "
                            "grep prefix | "
                            "restic -r repo/dir-at-repo -v backup --stdin")]]
         (cut/backup-role-command {:restic-repository "repo"
                                   :backup-path "dir-at-repo"
                                   :days-to-keep 39
                                   :months-to-keep 3
                                   :pg-host "localhost"
                                   :pg-port 5432
                                   :pg-db "mydb"
                                   :pg-role-prefix "prefix"
                                   :pg-user "user"
                                   :pg-password "password"}))))

(deftest should-calculate-backup-db-command
  (is (= [["bash" "-c" (str "pg_dump -h localhost -p 5432 -d mydb -U user --no-password "
                            "--serializable-deferrable | "
                            "restic -r repo/dir-at-repo -v backup --stdin")]]
         (cut/backup-db-command {:restic-repository "repo"
                                 :backup-path "dir-at-repo"
                                 :days-to-keep 39
                                 :months-to-keep 3
                                 :pg-host "localhost"
                                 :pg-port 5432
                                 :pg-db "mydb"
                                 :pg-user "user"
                                 :pg-password "password"}))))
