(ns dda.backup.core
  "Part of API, represents the application service layer."
  (:require
   [orchestra.core :refer [defn-spec]]
   [clojure.spec.alpha :as s]
   [dda.backup.infrastructure :as i]))

(def default {:dry-run false
              :debug false})

(s/def ::execution
  (s/keys :req-un []
          :opt-un [::dry-run 
                   ::debug
                   ::execution-directory]))

(s/def ::aws-access-key-id string?)
(s/def ::aws-secret-access-key string?)
(s/def ::aws-config
  (s/keys :req-un [::aws-access-key-id
                   ::aws-secret-access-key]))

(defn-spec env-or-file string?
  [name string?]
  (let [from-env (System/getenv name)
        name-from-file (System/getenv (str name "_FILE"))]
    (cond 
      (some? from-env) from-env
      (some? name-from-file) (slurp name-from-file)
      :else (throw ( RuntimeException. 
                    (str "Environment: [" name "," name-from-file "] was missing." ))))))


(defn-spec create-aws-credentials! nil?
  [config ::aws-config]
  (let [{:keys [aws-access-key-id aws-secret-access-key]} config]
    (i/execute! [["mkdir" "-p" "/root/.aws"]] {})
    (spit "/root/.aws/credentials"
          (str "[default]\n"
               "aws_access_key_id=" aws-access-key-id "\n"
               "aws_secret_access_key=" aws-secret-access-key "\n"))
    (i/execute! [["chmod" "0600" "/root/.aws/credentials"]] {})))