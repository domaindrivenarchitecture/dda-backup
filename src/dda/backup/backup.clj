(ns dda.backup.backup
  "Part of API, represents the application service layer."
  (:require
   [orchestra.core :refer [defn-spec]]
   [clojure.spec.alpha :as s]
   [dda.backup.backup.domain :as domain]
   [dda.backup.restic :as restic]
   [dda.backup.postgresql :as pg]
   [dda.backup.infrastructure :as i]))

(def default
  (merge restic/default
         pg/default))

(s/def ::backup-file-config
  (s/merge ::restic/restic-config
           (s/keys :req-un [::domain/files]
                   :opt-un [])))

(s/def ::pg-role-dump-config
  (s/merge ::pg/pg-config
           ::restic/restic-config
           (s/keys :req-un [::domain/pg-role-prefix]
                   :opt-un [])))

(s/def ::pg-db-dump-config
  (s/merge ::pg/pg-config
           ::restic/restic-config))

(defn- config-w-defaults
  [config]
  (if (restic/use-new-password? config)
    (merge default config {:password-file (:new-password-file config)})
    (merge default config)))

(defn-spec backup-file! nil?
  [config ::backup-file-config]
  (let [config-2-use (config-w-defaults config)]
    (restic/unlock! config-2-use)
    (i/execute!
     (domain/backup-files-command config-2-use)
     config-2-use)
    (restic/forget! config-2-use)))

(defn-spec backup-db-roles! nil?
  [config ::pg-role-dump-config]
  (let [config-2-use (config-w-defaults config)]
    (restic/unlock! config-2-use)
    (i/execute! (domain/backup-role-command config-2-use) config-2-use)
    (restic/forget! config-2-use)))

(defn-spec backup-db! nil?
  [config ::pg-db-dump-config]
  (let [config-2-use (config-w-defaults config)]
    (restic/unlock! config-2-use)
    (i/execute! (domain/backup-db-command config-2-use) config-2-use)
    (restic/forget! config-2-use)))