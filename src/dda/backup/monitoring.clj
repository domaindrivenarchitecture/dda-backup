(ns dda.backup.monitoring
  (:require
   [orchestra.core :refer [defn-spec]]
   [clojure.spec.alpha :as s]
   [dda.backup.monitoring.domain :as domain]
   [dda.backup.infrastructure :as i]))

(def default {:url "http://prometheus-pushgateway.monitoring.svc.cluster.local:9091/metrics/job"
              :namespace "default"
              :metrics {:kube_job_status_active 0
                        :kube_job_status_failed 0
                        :kube_job_status_succeeded 0}})

(s/def ::config (s/keys :req-un [::name]
                        :opt-un [::url ::metrics ::namespace]))

(defn- config-w-defaults
  [config]
    (merge default config))

(defn-spec send-metrics! nil?
  [config ::config]
  (let [config-2-use (config-w-defaults config)
        {:keys [url name]} config-2-use]
    (try 
      (i/post! (str url "/" name) (domain/collect-metrics config-2-use))
      (catch Exception e (println (str "Warn: unable to send log" (.getMessage e)))))))

(defn-spec backup-start-metrics! nil?
  [config ::config]
  (send-metrics!
   {:name "backup"
    :metrics {:kube_job_status_active 1
              :kube_job_status_start_time (long (/ (System/currentTimeMillis) 1000))}}))

(defn-spec backup-success-metrics! nil?
  [config ::config]
  (send-metrics!
   {:name "backup"
    :metrics {:kube_job_status_succeeded 1
              :kube_job_status_completion_time (long (/ (System/currentTimeMillis) 1000))}}))

(defn-spec backup-fail-metrics! nil?
  [config ::config]
  (send-metrics!
   {:name "backup"
    :metrics {:kube_job_status_failed 1
              :kube_job_status_completion_time (long (/ (System/currentTimeMillis) 1000))}}))

