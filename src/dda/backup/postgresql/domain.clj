(ns dda.backup.postgresql.domain
  "Intended for internal use only, represents the domain layer."
  (:require
   [orchestra.core :refer [defn-spec]]
   [clojure.spec.alpha :as s]
   [dda.backup.core.domain :as cd]
   [dda.backup.restic.domain :as rd]))

(s/def ::pg-host string?)
(s/def ::pg-port pos-int?)
(s/def ::pg-db string?)
(s/def ::pg-user string?)
(s/def ::pg-password string?)
(s/def ::pg-role-prefix string?)

(s/def ::pg-config
  (s/keys :req-un [::pg-host
                   ::pg-port
                   ::pg-db
                   ::pg-password
                   ::pg-user]
          :opt-un []))

(s/def ::pg-role-dump-config
  (s/merge ::pg-config
           ::rd/restic-config
           (s/keys :req-un [::pg-role-prefix]
                   :opt-un [])))

(s/def ::pg-db-dump-config
  (s/merge ::pg-config
           ::rd/restic-config))

(defn-spec psql-command ::cd/command
  [config ::pg-config
   command ::cd/command]
  (let [{:keys [pg-host pg-port pg-db pg-user]} config]
    (into
     ["psql" "-d" pg-db "-h" pg-host "-p" (str pg-port) "-U" pg-user
      "--no-password"]
     command)))

(defn-spec pgdumpall-command ::cd/command
  [config ::pg-role-dump-config
   command ::cd/command]
  (let [{:keys [pg-host pg-port pg-user pg-role-prefix]} config]
    (into
     []
     (concat
      ["pg_dumpall" "-h" pg-host "-p" (str pg-port) "-U" pg-user
       "--no-password"]
      command
      ["|" "grep" pg-role-prefix "|"]
      (rd/repo-command config ["backup" "--stdin"] false)))))

(defn-spec pgdump-command ::cd/command
  [config ::pg-db-dump-config
   command ::cd/command]
  (let [{:keys [pg-host pg-port pg-db pg-user]} config]
    (into
     []
     (concat
      ["pg_dump" "-h" pg-host "-p" (str pg-port) "-d" pg-db  "-U" pg-user
       "--no-password"]
      command
      ["|"]
      (rd/repo-command config ["backup" "--stdin"] false)))))

(defn-spec pgpass string?
  [config ::pg-config]
  (let [{:keys [pg-host pg-port pg-db pg-user pg-password]} config]
    (str pg-host ":" pg-port ":" pg-db ":" pg-user ":" pg-password "\n" 
         pg-host ":" pg-port ":template1:" pg-user ":" pg-password "\n")))

(defn-spec db-drop-command ::cd/commands
  [config ::pg-config]
  (let [{:keys [pg-db]} config
        config-w-template (merge config  {:pg-db "template1"})]
    [(psql-command config-w-template ["-c" (str "DROP DATABASE \"" pg-db "\";")])]))

(defn-spec db-create-command ::cd/commands
  [config ::pg-config]
  (let [{:keys [pg-db]} config
        config-w-template (merge config  {:pg-db "template1"})]
    [(psql-command config-w-template ["-c" (str "CREATE DATABASE \"" pg-db "\";")])]))