(ns dda.backup.monitoring.domain
  (:require
   [orchestra.core :refer [defn-spec]]
   [clojure.spec.alpha :as s]
   [clojure.string :as st]))

(s/def ::url string?)
(s/def ::name string?)
(s/def ::namespace string?)
(s/def ::metrics map?)

(s/def ::config (s/keys :req-un [::url ::name ::metrics ::namespace]))

(defn-spec collect-metrics string?
  [config ::config]
  (let [{:keys [metrics namespace]} config]
    (str
     (->> metrics
          (map (fn [entry] (str (name (key entry))
                                "{namespace=\"" namespace "\"}" " "
                                (val entry))))
          (st/join "\n"))
     "\n")))