(ns dda.backup.backup.domain
  "Intended for internal use only, represents the domain layer."
  (:require
   [orchestra.core :refer [defn-spec]]
   [clojure.spec.alpha :as s]
   [clojure.string :as st]
   [dda.backup.core.domain :as cd]
   [dda.backup.restic.domain :as rd]
   [dda.backup.postgresql.domain :as pd]))

(s/def ::files (s/+ string?))
(s/def ::pg-role-prefix ::pd/pg-role-prefix)

(s/def ::backup-file-config
  (s/merge ::rd/restic-config
           (s/keys :req-un [::files])))

(defn-spec backup-files-command ::cd/commands
  [config ::backup-file-config]
  (let [{:keys [files]} config]
    [(rd/repo-command config (into ["backup"] files) false)]))

(defn-spec backup-role-command ::cd/commands
  [config ::pd/pg-role-dump-config]
  [["bash" "-c" (st/join " " (pd/pgdumpall-command config ["--roles-only"]))]])

(defn-spec backup-db-command ::cd/commands
  [config ::pd/pg-db-dump-config]
  [["bash" "-c" (st/join " " (pd/pgdump-command config ["--serializable-deferrable"]))]])
