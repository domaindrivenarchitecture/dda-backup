(ns dda.backup.restore.domain
  "Intended for internal use only, represents the domain layer."
  (:require
   [orchestra.core :refer [defn-spec]]
   [clojure.spec.alpha :as s]
   [clojure.string :as st]
   [dda.backup.core.domain :as cd]
   [dda.backup.restic.domain :as rd]
   [dda.backup.postgresql.domain :as pd]))

(s/def ::restore-target-directory string?)
(s/def ::snapshot-id string?)
(s/def ::clean-up-element string?)
(s/def ::clean-up-elements (s/coll-of ::clean-up-element))

(s/def ::restore-file-config
  (s/merge ::rd/restic-config
           (s/keys :req-un [::restore-target-directory ::snapshot-id]
                   :opt-un [::clean-up-elements])))

(s/def ::restore-db-config
  (s/merge ::pd/pg-config
           (s/keys :req-un [::snapshot-id])))


(defn-spec restore-dir-command ::cd/commands
  [config ::restore-file-config]
  (let [{:keys [restore-target-directory snapshot-id clean-up-elements]} config]
    [(if (contains? config :clean-up-elements)
       (into 
        ["rm" "-rf"] 
        (map #(str restore-target-directory "/" %) clean-up-elements))
       ["rm" "-rf" restore-target-directory])
     (rd/repo-command config ["restore" snapshot-id "--target" restore-target-directory] false)]))

(defn-spec restore-db-command ::cd/commands
  [config ::restore-db-config]
  (let [{:keys [snapshot-id]} config]
    [["bash" "-c"
      (st/join
       " "
       (into
        []
        (concat
         (rd/repo-command config ["dump" snapshot-id "stdin"] false)
         ["|"]
         (pd/psql-command config []))))]]))
         
(defn-spec restore-db-roles-command ::cd/commands
  [config ::restore-db-config]
  (let [{:keys [snapshot-id]} config]
    [["bash" "-c"
      (st/join
       " "
       (into
        []
        (concat
         (rd/repo-command config ["dump" snapshot-id "stdin"] false)
         ["|"]
         (pd/psql-command (merge config {:pg-db "template1"}) []))))]]))
