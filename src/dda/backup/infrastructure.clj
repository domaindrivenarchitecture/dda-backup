(ns dda.backup.infrastructure
  "Intenden for internal use, represents the infrastructure layer."
  (:require [orchestra.core :refer [defn-spec]]
            [babashka.tasks :as t]
            [babashka.http-client :as http]
            [dda.backup.core.domain :as core]))

(defn-spec execute-out! string?
  [command ::core/command
   config ::core/execution]
  (let [{:keys [dry-run debug]} config]
    (when debug
      (println command))
    (when-not dry-run
      (:out (t/shell {:out :string :err :string} (clojure.string/join " " command))))))

(defn-spec execute-single! string?
  [command ::core/command
   config ::core/execution]
  (let [{:keys [dry-run debug]} config]
    (when debug
      (println command))
    (when-not dry-run
      (:out (t/shell {:err :string} (clojure.string/join " " command))))))


(defn-spec execute! nil?
  [commands ::core/commands
   config ::core/execution]
  (let [{:keys [dry-run debug]} config]
    (doseq [c commands]
      (when debug
        (println c))
      (when-not dry-run
        (apply t/shell c)))))

(defn-spec post! nil?
  [url string?
   content string?]
  (http/post url {:body content}))