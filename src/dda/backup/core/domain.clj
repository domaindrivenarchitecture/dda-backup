(ns dda.backup.core.domain
  "Intended for internal use only, represents the domain layer."
  (:require
   [clojure.spec.alpha :as s]))

(s/def ::command (s/cat
                  :app string?
                  :params (s/* string?)))
(s/def ::commands (s/coll-of ::command))

(s/def ::dry-run boolean?)
(s/def ::debug boolean?)
(s/def ::execution-directory string?)

(s/def ::execution
  (s/keys :req-un [::dry-run ::debug]
          :opt-un [::execution-directory]))
