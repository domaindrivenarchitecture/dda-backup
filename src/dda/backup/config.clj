(ns dda.backup.config
  "Part of API, represents the application service layer."
  (:require [aero.core :as aero]
            [dda.backup.core :as bc]
            [dda.backup.infrastructure :as i]))

(defmethod aero/reader 'env-or-file
  [{:keys [profile] :as opts} tag value]
  (bc/env-or-file value))

(defmethod aero/reader 'gopass
  [{:keys [profile] :as opts} tag value]
  (i/execute-out! (into ["gopass" "show" "-y" "-o"] value) {}))

(defn read-config 
  [file]
  (try 
    (aero/read-config file)
    (catch Exception e 
      (do (println (str "Warn: " e))
          {}))
    ))
