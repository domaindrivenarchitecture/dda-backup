(ns dda.backup.restic.domain
  "Intended for internal use only, represents the domain layer."
  (:require
   [orchestra.core :refer [defn-spec]]
   [clojure.spec.alpha :as s]
   [dda.backup.core.domain :as cd]))

(s/def ::certificate-file string?)
(s/def ::password-file string?)
(s/def ::new-password-file string?)
(s/def ::restic-repository string?)
(s/def ::backup-path string?)
(s/def ::days-to-keep pos?)
(s/def ::months-to-keep pos?)

(s/def ::restic-config
  (s/keys :req-un [::restic-repository
                   ::backup-path
                   ::days-to-keep
                   ::months-to-keep]
          :opt-un [::certificate-file
                   ::password-file
                   ::new-password-file
                   ::cd/execution-directory]))

(s/def ::check-error #{:not-initialized :wrong-password :no-password :unknown})

(defn-spec repo-command ::cd/command
  [config ::restic-config
   command ::cd/command
   read-error-stream boolean?]
  (let [{:keys [certificate-file password-file execution-directory
                restic-repository backup-path]} config
        shell-option (if read-error-stream {:err :string} {})]
    (into
     []
     (concat
      (cond
        (some? execution-directory)
        [(merge shell-option {:dir execution-directory})]
        read-error-stream
        [shell-option]
        :default [])
      ["restic" "-r" (str restic-repository "/" backup-path) "-v"]
      (cond
        (some? certificate-file)
        ["--cacert" certificate-file]
        (some? password-file)
        ["--password-file" password-file]
        :else
        [])
      command))))

(defn-spec check-repo-command ::cd/commands
  [config ::restic-config]
  [(repo-command config ["check"] true)])

(defn-spec init-repo-command ::cd/commands
  [config ::restic-config]
  [(repo-command config ["init"] false)])

(defn-spec unlock-repo-command ::cd/commands
  [config ::restic-config]
  [(repo-command config ["--cleanup-cache" "unlock"] false)])

(defn-spec list-snapshot-command ::cd/commands
  [config ::restic-config]
  [(repo-command config ["snapshots"] false)])

(defn-spec forget-command ::cd/commands
  [config ::restic-config]
  (let [{:keys [days-to-keep months-to-keep]} config]
    [(repo-command config ["forget" "--group-by" ""
                           "--keep-last" "1"
                           "--keep-daily" (str days-to-keep)
                           "--keep-monthly" (str months-to-keep) "--prune"] false)]))

(defn-spec change-password-command ::cd/command
  [config ::restic-config]
  (if (contains? config :new-password-file)
    (let [{:keys [new-password-file]} config]
      [(repo-command config ["--new-password-file" new-password-file
                             "key" "passwd"] false)])
    (throw (Exception. "change-password: new password required"))))

(defn-spec parse-check-error ::check-error
  [error string?]
  (cond
    (clojure.string/includes? error "Fatal: unable to open config file") :not-initialized
    (clojure.string/includes? error "Fatal: wrong password or no key found") :wrong-password
    (clojure.string/includes? error "Resolving password failed") :no-password
    :default :unknown))
