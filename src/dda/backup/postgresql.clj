(ns dda.backup.postgresql
  "Part of API, represents the application service layer."
  (:require
   [orchestra.core :refer [defn-spec]]
   [clojure.spec.alpha :as s]
   [dda.backup.postgresql.domain :as domain]
   [dda.backup.core :as core]
   [dda.backup.infrastructure :as i]))

(def default
  (merge core/default
         {:pg-host "localhost"
          :pg_port 5432}))

(s/def ::pg-config
  (s/keys :req-un [::domain/pg-db
                   ::domain/pg-user
                   ::domain/pg-password]
          :opt-un [::domain/pg-host
                   ::domain/pg-port]))

(defn-spec create-pg-pass! nil?
  [config ::pg-config]
  (let [config-w-defaults (merge default config)]
    (spit "/root/.pgpass" (domain/pgpass config-w-defaults))
    (i/execute! [["chmod" "0600" "/root/.pgpass"]] config)))

(defn-spec drop-create-db! nil?
  [config ::pg-config]
  (let [config-w-defaults (merge default config)]
    (try (i/execute! (domain/db-drop-command config-w-defaults) config-w-defaults)
         (catch Exception e (println (.getMessage e))))
    (i/execute! (domain/db-create-command config-w-defaults) config-w-defaults)))
