(ns dda.backup.restore
  "Part of API, represents the application service layer."
  (:require
   [orchestra.core :refer [defn-spec]]
   [clojure.spec.alpha :as s]
   [dda.backup.restore.domain :as domain]
   [dda.backup.restic :as restic]
   [dda.backup.postgresql :as pg]
   [dda.backup.infrastructure :as i]))

(def default (merge restic/default 
                    pg/default
                    {:snapshot-id "latest"}))

(s/def ::restore-file-config
  (s/merge ::restic/restic-config
           (s/keys :req-un [::domain/restore-target-directory]
                   :opt-un [::domain/snapshot-id])))

(s/def ::restore-db-config
  (s/merge ::pg/pg-config
           (s/keys :req-un [::domain/snapshot-id])))

(defn- config-w-defaults
  [config]
  (if (restic/use-new-password? config)
    (merge default config {:password-file (:new-password-file config)})
    (merge default config)))


(defn-spec restore-file! nil?
  [config ::restore-file-config]
  (let [config-2-use (config-w-defaults config)]
    (restic/unlock! config-2-use)
    (i/execute!
     (domain/restore-dir-command config-2-use)
     config-2-use)))

(defn-spec restore-db! nil?
  [config ::restore-db-config]
  (let [config-2-use (config-w-defaults config)]
    (restic/unlock! config-2-use)
    (i/execute! (domain/restore-db-command config-2-use) config-2-use)))

(defn-spec restore-db-roles! nil?
  [config ::restore-db-config]
  (let [config-2-use (config-w-defaults config)]
    (restic/unlock! config-2-use)
    (i/execute! (domain/restore-db-roles-command config-2-use) config-2-use)))
