(ns dda.backup.restic
  "Part of API, represents the application service layer."
  (:require
   [orchestra.core :refer [defn-spec]]
   [clojure.spec.alpha :as s]
   [dda.backup.restic.domain :as domain]
   [dda.backup.core :as core]
   [dda.backup.infrastructure :as i]))

(def default
  (merge core/default
         {:days-to-keep 30
          :months-to-keep 12}))

(s/def ::restic-config
  (s/merge ::core/execution
           (s/keys :req-un [::domain/restic-repository
                            ::domain/backup-path]
                   :opt-un [::domain/certificate-file
                            ::domain/password-file
                            ::domain/new-password-file
                            ::domain/days-to-keep
                            ::domain/months-to-keep])))

(s/def ::check-result #{:initialized :wrong-password :not-initialized :error})

(defn-spec check ::check-result
  "Check a restic repository whether it is initialized and there are correct credentials given."
  [restic-config ::restic-config]
  (let [config-w-defaults (merge core/default restic-config)]
    (try
      (i/execute! (domain/check-repo-command config-w-defaults) config-w-defaults)
      :initialized
      (catch Exception e
        (let [data (ex-data e)
              parsed-error (domain/parse-check-error (get-in data [:proc :err]))]
          (cond
            (= parsed-error :not-initialized) :not-initialized
            (= parsed-error :wrong-password) :wrong-password
            :default :error))))))

(defn-spec use-new-password? boolean?
  "Check, whether a given new password should b used instead of the normal one."
  [restic-config ::restic-config]
  (if (contains? restic-config :new-password-file)
    (= :initialized (check (merge restic-config {:password-file (:new-password-file restic-config)})))
    false))

(defn- config-w-defaults
  "Merge given configuration with defaults & replace restic-password with restic-new-password if necessary."
  [restic-config]
  (if (use-new-password? restic-config)
    (merge default restic-config {:password-file (:new-password-file restic-config)})
    (merge default restic-config)))

(defn-spec
  ^{:deprecated "0.2.0"
    :superseded-by "check"}
  initalized? boolean?
  [restic-config ::restic-config]
  (let [config-2-use (config-w-defaults restic-config)]
    (= :initialized (check config-2-use))))

(defn-spec init! nil?
  [restic-config ::restic-config]
  (let [config-2-use (config-w-defaults restic-config)]
    (when (= :not-initialized (check config-2-use))
      (i/execute! (domain/init-repo-command config-2-use) config-2-use))))

(defn-spec unlock! nil?
  [restic-config ::restic-config]
  (let [config-2-use (config-w-defaults restic-config)]
    (i/execute! (domain/unlock-repo-command config-2-use) config-2-use)))

(defn-spec forget! nil?
  [restic-config ::restic-config]
  (let [config-2-use (config-w-defaults restic-config)]
    (i/execute! (domain/forget-command config-2-use) config-2-use)))

(defn-spec list-snapshots! nil?
  [restic-config ::restic-config]
  (let [config-2-use (config-w-defaults restic-config)]
    (i/execute! (domain/list-snapshot-command config-2-use) config-2-use)))

(defn-spec change-password! nil?
  [restic-config ::restic-config]
  (when (contains? restic-config :new-password-file)
    (let [config-2-use (merge core/default restic-config)]
      (when (= :initialized (check config-2-use))
        (do
          (i/execute! (domain/change-password-command config-2-use) config-2-use)
          (when-not (= :wrong-password (check config-2-use))
            (throw (Exception. "password-change did not work!"))))))))
